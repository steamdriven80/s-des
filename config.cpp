#include <fstream>
#include <iostream>
#include <stdlib.h>

#include "config.h"

#include "logger.h"

int LookupTable::performLookup(u32 index) {

    if (index >= permutation.length()) {
        Logger::log(LOGLEVEL_ERROR, "LookupTable::performLookup -> Attempting to access out of bounds index");

#ifdef DEBUG
                throw( "attempting to access outofbounds index" );
    #endif

        return 0;
    }

    char value = permutation[index] - '0';

    if (value >= 0 && value < 10) return value;

    value = permutation[index] - 'A';

    if (value >= 0 && value < 10) return value;

    value = permutation[index] - 'a';

    if (value >= 0 && value < 10) return value;

    // should never reach here

    Logger::log(LOGLEVEL_WARNING, "LookupTable::performLookup -> invalid table entry detected: ASCII Code " + itoa( permutation[index] ) + "\n" );

    return 0;
}

u32  LookupTable::permute(u32 value) {
    u32 finalVal = 0;
    u32 len = permutation.length();
    char buf[2];
    buf[1] = '\0';

    int spot;

    for (u32 i = 0; i < len; i++) {
        buf[0] = permutation[i];
        spot = strtoul(buf, NULL, 16);

        bool flag = (value & (1 << (spot - 1)));

        finalVal |= (flag) ? (1 << i) : 0;
    }

    return finalVal;
}

u32 LookupMatrix::performLookup(u32 row, u32 col) {
    LookupTable table;
    table.permutation = tableRows[row];

    return table.performLookup(col);
}

u32 LookupMatrix::cherryPick(u32 value) {

    u32 row = 0, col = 0;

    // this loop iterates over all indices of the permutation table for row lookups
    // it tests value to see if that bit is set, and if so, it sets the appropriate bit
    // in the final value
    for (u32 index = 0; index < rowLookup.permutation.length(); index++) {
        row |= (value & (1 << (rowLookup.performLookup(index) - 1))) ? (1 << index) : 0;
    }

    // does a similar function but for the column value
    for (u32 index = 0; index < colLookup.permutation.length(); index++) {
        col |= (value & (1 << (colLookup.performLookup(index) - 1))) ? (1 << index) : 0;
    }

    // actually looks up the table value for the passed in value
    return performLookup(row, col);
}

std::string rtrim(std::string str) {
    // trim trailing spaces
    size_t endpos = str.find_last_not_of(" \t");
    if (std::string::npos != endpos) {
        str = str.substr(0, endpos + 1);
    }

    return str;
}

std::string ltrim(std::string str) {
    // trim trailing spaces
    size_t endpos = str.find_first_not_of(" \t");
    if (std::string::npos != endpos) {
        str = str.substr(endpos, str.length() - endpos);
    }

    return str;
}

template<typename T, typename P>
T remove_if(T beg, T end, P pred) {
    T dest = beg;
    for (T itr = beg; itr != end; ++itr)
        if (!pred(*itr))
            *(dest++) = *itr;
    return dest;
}

std::string Config::calculateInverse(std::string permute) {

    u32 len = permute.length();
    char buf[2];
    buf[1] = '\0';
    char *table = new char[len + 1];

    table[len] = '\0';
    int value;

    for (u32 i = 0; i < len; i++) {
        buf[0] = permute[i];
        value = strtoul(buf, NULL, 16);
        table[value - 1] = i + '1';
    }

    std::string inv(table);

    delete[] table;

    return inv;
}

Config::Config() {

}

Config::~Config() {

}

bool Config::load(std::string fileName) {

    int sbox_line = 0;
    int sbox = 0;
    int sbox_maxrow = 0;

    // the parameter file we've loaded
    fileName = fileName;

    int state = 0;
    int tmp2;
    std::string line;
    std::ifstream myfile(fileName.c_str(), std::ios::in);
    if (myfile.is_open()) {
        while (getline(myfile, line) && state != 16) {
            std::size_t comment = line.find("//");

            if (comment != std::string::npos)
                line.replace(comment, line.length() - comment, "");

            line.erase(remove_if(line.begin(), line.end(), isspace), line.end());

            char buff[8];

            if (line.length() < 1 || line[0] == ' ' || line[0] == '\n') {
                continue;
            }

            sprintf(buff, "%d", state);

            // Logger::log(LOGLEVEL_DEBUG, std::string("config parsed line: ") + line + ", state: " + std::string(buff) + "\n" );


            switch (state) {
                case 0: // block size

                    blockSize = atoi(line.c_str());

                    state++;

                    break;

                case 1: // key size

                    keySize = atoi(line.c_str());

                    state++;

                    break;

                case 2: // effective key size

                    effectiveKeySize = atoi(line.c_str());

                    state++;

                    break;

                case 3: // round key size

                    roundKeySize = atoi(line.c_str());

                    state++;

                    break;

                case 4: // number of rounds

                    roundCount = atoi(line.c_str());

                    state++;

                    break;

                case 5: // PC-1

                    pcRoundInitial.permutation = line;

                    state++;

                    break;

                case 6: // PC-2

                    pcRoundKey.permutation = line;

                    state++;

                    break;

                case 7: // left rotation schedule

                    leftShiftSchedule.permutation = line;

                    state++;

                    break;

                case 8: // initial permutation

                    initialPermute.permutation = line;
                    inverseIP.permutation = calculateInverse(line);

                    state++;

                    break;

                case 9: // expansion permutation

                    expansionPermute.permutation = line;

                    state++;

                    break;

                case 10: // # of s-boxes

                    sboxCount = atoi(line.c_str());

                    state++;

                    break;

                case 11: // row selection bits

                    row.permutation = line;

                    state++;

                    break;

                case 12: // col selection bits

                    col.permutation = line;

                    state++;

                    break;

                case 13: // pbox transposition

                    pBox.permutation = line;

                    state++;

                    break;

                case 14:

                    sbox_line = 0;
                    sbox = 0;
                    sBoxes = new LookupMatrix[sboxCount];
                    tmp2 = (row.permutation.length());
                    sbox_maxrow = 2;
                    for (int p = 1; p < tmp2; p++) sbox_maxrow *= 2;

                    state++;

                    if (sboxCount) {
                        sBoxes[0].rowLookup = row;
                        sBoxes[0].colLookup = col;
                    }

                case 15:

                    sBoxes[sbox].tableRows.push_back(line);

                    sbox_line++;

                    if (sbox_line >= sbox_maxrow) {
                        sbox++;
                        sbox_line = 0;

                        state = 15;

                        if (sbox >= sboxCount) {
                            state++;
                        } else {
                            sBoxes[sbox].rowLookup = row;
                            sBoxes[sbox].colLookup = col;
                        }
                    }

                    break;

                case 16:
                default:

                    Logger::log(LOGLEVEL_WARNING, "reached unreachable code in config file parser\n");

                    return false;

                    break;
            }
        }
        myfile.close();
    }
    else {
        Logger::log(LOGLEVEL_ERROR, "invalid file passed to Config::load()\n");
        return false;
    }

    return true;
}

void Config::close() {

}
