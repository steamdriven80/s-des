#ifndef _MANGLER_H_
#define _MANGLER_H_

/**
*   mangler.h
*
*   Chris Pergrossi - CIS4362
*
* handles the mangle function, as well as applying permutations, transpostions,
* etc. for all rounds of the encryption/decryption operation.
*
***/

#include "config.h"
#include "keygen.h"
#include "optparse.h"
#include "hexbin.h"
#include "logger.h"
#include "streamio.h"

class Mangler {
protected:

    friend void Mangler_Tests(int argc, char* argv[]);

    int m_blockSize;
    char *m_latestBlock;

    Config m_config;
    OptParse m_options;
    KeyGen m_keys;

public:

    Mangler();

    ~Mangler();

    void init(int argc, char *argv[]);

    void release();

    void setMasterKey(unsigned int key);

    u32 encryptBlock(u32 Block);

    u32 decryptBlock(u32 Block);
};

#endif
