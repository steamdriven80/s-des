#include "config.h"
#include "recheck.h"

void Config_Tests() {

    stack("Config File Tests");

    Config C;

    C.load("test-params.txt");

    it("should load blockSize == 8");

        expect(C.blockSize).toBeEqual(8);


    it("should load keySize == 10");

        expect(C.keySize).toBeEqual(10);


    it("should load effectiveKeySize == 10");

        expect(C.effectiveKeySize).toBeEqual(10);


    it("should load roundKeySize == 8");

        expect(C.roundKeySize).toBeEqual(8);


    it("should load roundCount == 2");

        expect(C.roundCount).toBeEqual(2);


    it("should load initial Permute == 3,5,2,7,4,A,1,9,8,6");

        expect(C.pcRoundInitial.permutation).toBeEqual("35274A1986");


    it("should perform a permutation on a known value");

        u32 inter = C.pcRoundInitial.permute(0x2CE);

        expect(inter).toBeEqual(317);

    it("should load round Permute == 6,3,7,4,8,5,A,9");

        expect(C.pcRoundKey.permutation).toBeEqual("637485A9");


    it("should load leftShift Schedule == 1,2");

        expect(C.leftShiftSchedule.permutation).toBeEqual("12");


    it("should load initial permutatation == 2,6,3,1,4,8,5,7");

        expect(C.initialPermute.permutation).toBeEqual("26314857");


    it("should load inverse permutation == 4,1,3,5,7,2,8,6");

        C.inverseIP.permutation = C.calculateInverse(C.initialPermute.permutation);

        expect(C.inverseIP.permutation).toBeEqual("41357286");


    it("should load expansion permutation == 4,1,2,3,2,3,4,1");

        expect(C.expansionPermute.permutation).toBeEqual("41232341");


    it("should load # of sBoxes == 2");

        expect(C.sboxCount).toBeEqual(2);

    it("should cherry pick correctly");

        u32 value = C.sBoxes[0].cherryPick(0xA);

        expect(value).toBeEqual(2);
}
