#include "recheck.h"

// this must be implemented in a seperate file due to a cyclic dependency

bool _test::resolve() {
    if (parent) {parent->resolve(this);}
    return result;
}
