#ifndef _TESTS_H_
#define _TESTS_H_
//#ifdef _WITH_TESTS_

void OptParse_Tests();

void Config_Tests();

void StreamIO_Tests();

void KeyGen_Tests();

void Mangler_Tests(int argc, char* argv[]);

void conductTests( int argc, char* argv[] ) {
    OptParse_Tests();
    Config_Tests();
    StreamIO_Tests();
    KeyGen_Tests();
    Mangler_Tests(argc, argv);
}

//#endif
#endif
