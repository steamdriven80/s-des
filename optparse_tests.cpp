#include <iostream>

#include "recheck.h"
#include "optparse.h"

using namespace std;

void OptParse_Tests() {

    int argc = 7;
    std::string argv[] = {"./a.out", "-ihello.txt", "-ogood.txt", "-kAFEF3F31", "-psomeparams.txt", "-d", "-x"};

    cout << "S-DES Project - CIS4362 - Spring 2015" << endl;
    cout << "Chris Pergrossi" << endl << endl;

    cout << "Usage: s-des [OPTIONS]" << endl << endl;

    cout << "Options:" << endl;
    cout << "          -i<filename>     the input file to use, '-' for STDIN" << endl;
    cout << "          -o<filename>     the output file to use, '-' for STDOUT" << endl;
    cout << "          -e               encrypt the input, the default." << endl;
    cout << "          -d               decrypt the input, exclusive with -e." << endl;
    cout << "          -k<key>          the encryption key, to be given in hexadecimal" << endl;
    cout << "          -p<filename>     the parameter file to read for configuration" << endl;
    cout << "          -s               verbose mode, with each step output to console" << endl;
    cout << "          -x               hexadecimal mode, indicating input,output, and console" << endl;;
    cout << "                           should be interpreted in hex rather than binary." << endl;
    cout << endl;
    cout.flush();

    stack("OptParse Unit Tests");

    it("should encrypt by default");

        OptParse O;

        expect(O.Defaults.encrypt).toBeEqual(true);

    it("should not have decrypt selected by default");

        OptParse O;

        expect(O.Defaults.decrypt).toBeEqual(false);

    it("should input from file 'ptxt'");

        OptParse O;

        expect(O.Defaults.inFile).toBeEqual("ptxt");

    it("should output to file 'ctxt'");

        OptParse O;

        expect(O.Defaults.outFile).toBeEqual("ctxt");

    it("should use keyfile 'key.txt'");

        OptParse O;

        expect(O.Defaults.key).toBeEqual("key.txt");

    it("should not have loaded the key by default");

        OptParse O;

        expect(O.Defaults.keyLoaded).toBeEqual(false);

    it("load a set of argv and parse them : keyLoaded");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.keyLoaded).toBeEqual(true);

    it("load a set of argv and parse them : key");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.key).toBeEqual("AFEF3F31");

    it("load a set of args and parse them : inFile");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.inFile).toBeEqual("hello.txt");

    it("load a set of args and parse them : keyLoaded");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.outFile).toBeEqual("good.txt");

    it("load a set of args and parse them : paramFile");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.paramFile).toBeEqual("someparams.txt");

    it("load a set of args and parse them : decrypt");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.decrypt).toBeEqual(true);

    it("load a set of args and parse them : verbose");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.verbose).toBeEqual(true);

    it("load a set of args and parse them : hexadecimal");

        OptParse O;

        O.parse(argc, argv);

        expect(O.Parsed.hexadecimal).toBeEqual(true);

}

// int main(int argc, char *argv[]) {
//   OptParse_Tests(argc, argv);
//
//   return 0;
// }
