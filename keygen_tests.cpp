#include <string>

#include "optparse.h"

#include "recheck.h"
#include "keygen.h"
#include "config.h"

void KeyGen_Tests() {
    stack("KeyGen Tests");

    it("should interpret a key in memory");

        KeyGen K;

        u32 key = K.loadMaster("AB34");

        expect(key).toBeEqual(43828);

    it("should load the key from a file");

        KeyGen K;

        u32 key = K.loadMaster("test-key.txt");

        expect(key).toBeEqual(3007299795);

    OptParse O;
    Config C;

    C.load("test-params.txt");

    O.loadDefaults();

    it("should perform the initial permutation");

        KeyGen K;

        u32 key = K.loadMaster("test-key.txt") & 0xFF;

        u32 init = C.initialPermute.permute(key);

        char buf[22];

        sprintf(buf, "%u", init);

        _Test.setDesc("should permute a test value to " + std::string(buf));

        expect(init).toBeEqual(233);

    it("should generate round keys");

        KeyGen K;

        u32 key = K.loadMaster(O.options().key) & 0xFF;

        K.generate(key, C.roundCount, C.pcRoundInitial.permutation, C.leftShiftSchedule.permutation, C.pcRoundKey.permutation);

        expect(true).toBeEqual(true);
}
