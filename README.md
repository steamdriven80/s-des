# s-des
A simplified variation of the DES algorithm, intended for learning about cryptographic systems and block ciphers.  Run the program without any flags to see usage information.

# known bugs
The system does not function as intended with encryption and decryption, however, this is a logic flaw and not syntax or (I believe) design related.  Unfortunately, the provided spec sheet was very confusing and contained several errors, making this assignment open to interpretation, unfortunately.

examples of specification flaws:
o In the parameter file given, the indicated block size is 4 bits, instead of 8.  This is impossible, for example, when referncing into the s-box tables we need to use half-block pieces, and the s-box expects 4 bits itself.
o Emphasized throughout the document is the importance of referencing from '1' instead of from '0' (due to another document we're basing our work on) - however, the s-box tables are 0 based leaving an inconsistency for the programmer to find on his/her own.
o The manner in which the program is described is very convoluted and hard to follow.  The document claims to be based on another work (the original s-des design document) yet deviates severely and without indication.
o We were given a week to complete this project, which I feel was an extremely short timeline given the amount of code that needed to be written, tested, and documented.
