#include "recheck.h"

#include "mangler.h"

void Mangler_Tests(int argc, char* argv[]) {
    stack("Mangler Tests");

    it("should create a new object");

        Mangler M;

        M.init(argc, argv);

        u32 encrypted = M.encryptBlock(0xFE);

        printf("Encrypted: %x\n", encrypted);

        u32 decrypted = M.decryptBlock(encrypted);

        printf("Decrypted: %x\n", decrypted);

    expect(M.m_blockSize).toBeEqual(0);
}