#ifndef _HEXBIN_H_
#define _HEXBIN_H_

/**
*   hexbin.h
*
*   Chris Pergrossi - CIS4362
*
* a set of utility functions for converting, inputting and outputting
* ASCII binary and hexadecimal numbers.
*
***/

#include <string>

/**
* nint
*
* a small object to  aid in passing around arbitrary precision integers.
* we assume we will not be passing 32-bit precision (through specification)
*
***/

// converts string representations to one another
std::string sHex2sBin(std::string hex);

std::string sBin2sHex(std::string bin);

// converts string representations to binary
unsigned int sHex2Bin(std::string hex);

unsigned int sBin2Bin(std::string bin);

// converts binary representations to string
std::string Bin2sHex(unsigned int value);

std::string Bin2sBin(unsigned int value, int keep = 32);


#endif
