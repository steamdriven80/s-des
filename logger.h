#ifndef _LOGGER_H_
#define _LOGGER_H_

/**
*   logger.h
*
*   Chris Pergrossi - CIS4362
*
* this object handles pretty-printing to console in both ASCII binary and
* hexadecimal notations
*
**/

typedef enum {
    LOGLEVEL_DEBUG = 0,
    LOGLEVEL_VERBOSE = 1,
    LOGLEVEL_WARNING = 2,
    LOGLEVEL_ERROR = 3
} LogLevel;

#define ANSI_RED "\x1b[31m"
#define ANSI_GREEN "\x1b[32m"
#define ANSI_YELLOW "\x1b[33m"
#define ANSI_BLUE "\x1b[34m"
#define ANSI_TEAL "\x1b[36m"
#define ANSI_WHITE "\x1b[37m"
#define ANSI_RESET "\x1b[0m"

#include <string>

#define STD_COLOR(x) std::string(x)

std::string itoa(int value);

class Logger {
protected:

    static LogLevel m_loglevel;

public:

    static void setLogLevel(LogLevel newLevel) {
        Logger::m_loglevel = newLevel;
    }

    static void log(LogLevel level, std::string message);

};

#endif
