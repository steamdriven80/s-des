#include <stdio.h>
#include "recheck.h"

#include "streamio.h"

void StreamIO_Tests() {
    stack("StreamIO Tests");

    it("should load a file into memory : data");

        StreamIO obj("test-params.txt", false);

        expect(obj.m_data).toNotBeNull();

    it("should load a file into memory : ascii");

        StreamIO obj("test-params.txt", false);

        expect(obj.m_binary).toBeEqual(false);

    it("should load a file into memory : size");

        StreamIO obj("test-params.txt", false);

        char buf[8];

        sprintf(buf, "%d", obj.m_size);

        _Test.setDesc("file size > 0 : " + std::string(buf) + " bytes");

        expect(obj.m_size).toNotBeEqual(0);
}
