#include <iostream>
#include <stdio.h>
#include "logger.h"

using namespace std;

#ifdef DEBUG
LogLevel Logger::m_loglevel = LOGLEVEL_DEBUG;
#else
LogLevel Logger::m_loglevel = LOGLEVEL_WARNING;
#endif

std::string itoa(int value) {
    char buf[16];

    sprintf(buf, "%d", value);

    return std::string(buf);
}

void Logger::log(LogLevel level, std::string message) {
    if (level < Logger::m_loglevel) {
        return;
    }

    std::string color;

    switch (level) {
        case LOGLEVEL_DEBUG:
            color = ANSI_BLUE "[DEBUG]: " ANSI_RESET;
            break;
        case LOGLEVEL_VERBOSE:
            color = ANSI_TEAL " [INFO]: " ANSI_RESET;
            break;
        case LOGLEVEL_WARNING:
            color = ANSI_YELLOW " [WARN]: " ANSI_RESET;
            break;
        case LOGLEVEL_ERROR:
            color = ANSI_RED "[ERROR]: " ANSI_RESET;
            break;
    }

    cout << color << message << STD_COLOR(ANSI_RESET);
}
