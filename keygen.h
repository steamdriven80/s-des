#ifndef _KEYGEN_H_
#define _KEYGEN_H_

/**
*   keygen.h
*
*   Chris Pergrossi - CIS43632
*
* handles generating round keys from a master key.
*
***/

typedef unsigned int u32;

class KeyGen {
protected:

    u32 m_master;

    u32 *m_rounds;

    u32 m_numRounds;

public:

    KeyGen() : m_master(0), m_rounds(NULL), m_numRounds(0) {
    }

    ~KeyGen() {
        release();
    }

    // a catch-all key loading method.  Tests parameter for hexadecimal
    // only digits to treat as a key value, otherwise treats a a filename.
    // deciphers hexadecimal digits into a u32 value
    u32 loadMaster(std::string pathOrValue);

    // generates all the round keys from the passed in master key with provided functions
    void generate(u32 master, u32 numberRounds, std::string initialPermute, std::string rotSchedule, std::string roundPermute);

    // releases allocated memory
    void release();


    // returns the master key in various forms
    u32 getMasterBin() {
        return m_master;
    }

    std::string getMastersHex();

    std::string getMastersBin();

    u32 getRoundCount() {
        return m_numRounds;
    }

    // retrieves individual round keys in various forms
    u32 getRoundKeyBin(u32 index);

    std::string getRoundKeysHex(u32 index);

    std::string getRoundKeysBin(u32 index);
};

#endif
