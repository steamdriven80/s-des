#include <fstream>

#include "streamio.h"

// loads a file into memory
bool StreamIO::load(std::string fileName, bool isBinary) {

    // release any previously allocated memory
    close();

    // open the file and determine its size by seeking to the end
    std::streampos size;

    std::ios_base::openmode flags = std::ios::in | std::ios::ate;

    if (isBinary) {
        flags |= std::ios::binary;
    }

    m_binary = isBinary;
    m_fileName = fileName;

    std::ifstream file(fileName.c_str(), flags);
    if (file.is_open()) {
        m_size = file.tellg();

        // allocate the new file buffer and read the entire file
        m_data = new char[m_size];
        file.seekg(0, std::ios::beg);
        file.read(m_data, m_size);
        file.close();

        return true;
    }

    return false;      // an error occured
}

// frees memory allocated for a file
bool StreamIO::close() {
    if (m_data) {
        delete[] m_data;

        m_data = NULL;
        m_size = 0;
        m_fileName = "";
    }

    return true;
}
