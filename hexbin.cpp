#include <stdlib.h>
#include <stdio.h>
#include <bitset>

#define min(a, b) (((int)a>(int)b) ? b : a)

#include "hexbin.h"

// converts string representations to one another
std::string sHex2sBin(std::string hex) {
    return Bin2sBin(sHex2Bin(hex));
}

std::string sBin2sHex(std::string bin) {
    return Bin2sHex(sBin2Bin(bin));
}

// converts string representations to binary
unsigned int sHex2Bin(std::string hex) {
    return strtoul(hex.c_str(), NULL, 16);
}

unsigned int sBin2Bin(std::string bin) {
    return strtoul(bin.c_str(), NULL, 2);
}

// converts binary representations to string
std::string Bin2sHex(unsigned int value) {

    char buf[16];

    sprintf(buf, "%x", value);

    return std::string(buf);
}

std::string Bin2sBin(unsigned int value, int keep) {
    std::bitset<32> val(value);

    std::string ret = val.to_string();

    keep = min(keep, ret.length());

    return ret.substr(ret.length() - keep);
}
