GCC=`which g++`
SOURCE=$(wildcard *.cpp)
HEADERS=$(wildcard *.h)

all: proj1

proj1: $(SOURCE) $(HEADERS)
	g++ -Wall $(SOURCE)   -D DEBUG -o proj1
