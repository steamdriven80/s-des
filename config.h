#ifndef _CONFIG_H_
#define _CONFIG_H_

/**
*   config.h
*
*   Chris Pergrossi - CIS4362
*
* an object responsible for loading (and saving) program parameters from/to
* file.  provides an easy by-name method of retrieving the paramters and
* unifies the manner in which they're saved as well.
*
***/

#include <string>
#include <vector>

typedef unsigned int u32;

struct LookupTable {
    short outLength;         // the number of bits output
    short inLength;          // the number of bits input
    std::string permutation;         // the order and permutation/expansion/etc. of bits;

    int performLookup(u32 index);

    u32 permute(u32 value);       // expects permutation to be 1 based, not 0.
};

struct LookupMatrix {

    short outLength;    // the number of bits produced by the table lookup
    short inLength;     // the number of bits that can be 'cherry picked'
    std::vector<std::string> tableRows;        // the table values

    LookupTable rowLookup;          // the permutation choice for row lookup
    LookupTable colLookup;          // the permutation choice for col lookup

    u32 performLookup(u32 row, u32 col);   // performs a simple lookup using given row,col
    u32 cherryPick(u32 value);             // permutes bits according to parameters and
    // follows up with a table lookup.
};

class Config {
public:

    // simply too many variables to provide individual setters and getters.  I feel
    // it would add far more complexity/room for error than is warranted.

    // the parameter file we've loaded
    std::string fileName;

    // variable 'B'
    int blockSize;

    // variable 'Q'
    int keySize;

    // variable 'QE'
    int effectiveKeySize;

    // variable 'R'
    int roundKeySize;

    // variable 'N'
    int roundCount;

    // variable 'PC-1'
    LookupTable pcRoundInitial;

    // variable 'PC-2'
    LookupTable pcRoundKey;

    // variable 'RS'
    LookupTable leftShiftSchedule;

    // variable 'IP'
    LookupTable initialPermute;

    // variable 'IP-1'
    LookupTable inverseIP;

    // variable 'E'
    LookupTable expansionPermute;

    // variable 'T'
    int sboxCount;

    LookupTable row;

    LookupTable col;

    // combination of variables 'Row, Col, and Si'
    LookupMatrix *sBoxes;

    // variable 'P'
    LookupTable pBox;

public:

    Config();

    ~Config();

    std::string calculateInverse(std::string permute);

    bool load(std::string fileName);

    void close();
};

#endif
