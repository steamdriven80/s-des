#ifndef _OPTPARSE_H_
#define _OPTPARSE_H_

/**
*   optparse.h
*
*   Chris Pergrossi - CIS4362
*
* this object handles parse command line options, as well as setting default
* values if none is provided.
*
***/


#include <string>


struct Options {

    friend void OptParse_Tests();

    std::string inFile;
    std::string outFile;

    bool encrypt;
    bool decrypt;

    bool keyLoaded;    // indicates whether 'key' is a filename or a value
    std::string key;
    std::string paramFile;

    bool verbose;

    bool hexadecimal;
};

class OptParse {
public:

    // a set of default options
    Options Defaults;

    // the parsed and conclusive option list
    Options Parsed;

public:

    void loadDefaults(bool encrypt = true);    // handles filling the defaults options
    void printUsage();

    OptParse();

    ~OptParse();

    bool parse(int argc, std::string argv[]);

    bool parse(int argc, char *argv[]);

    Options &options() {
        return Parsed;
    }
};

#endif
