#include <iostream>
#include <string.h>
#include "optparse.h"

using namespace std;

void OptParse::loadDefaults(bool encrypt) {

    Defaults.encrypt = encrypt;
    Defaults.decrypt = !encrypt;

    Defaults.inFile = (encrypt) ? "ptxt" : "ctxt";
    Defaults.outFile = (encrypt) ? "ctxt" : "ptxt";

    Defaults.keyLoaded = false;
    Defaults.key = "key.txt";
    Defaults.paramFile = "params.txt";

#ifdef DEBUG
  Defaults.verbose   = true;
  #else
    Defaults.verbose = false;
#endif

    Defaults.hexadecimal = false;
}

OptParse::OptParse() {
    loadDefaults(true);

    Parsed = Defaults;
}

OptParse::~OptParse() {

}

void OptParse::printUsage() {

    cout << "S-DES Project - CIS4362 - Spring 2015" << endl;
    cout << "Chris Pergrossi" << endl << endl;

    cout << "Usage: s-des [OPTIONS]" << endl << endl;

    cout << "Options:" << endl;
    cout << "          -i<filename>     the input file to use, '-' for STDIN" << endl;
    cout << "          -o<filename>     the output file to use, '-' for STDOUT" << endl;
    cout << "          -e               encrypt the input, the default." << endl;
    cout << "          -d               decrypt the input, exclusive with -e." << endl;
    cout << "          -k<key>          the encryption key, to be given in hexadecimal" << endl;
    cout << "          -p<filename>     the parameter file to read for configuration" << endl;
    cout << "          -s               verbose mode, with each step output to console" << endl;
    cout << "          -x               hexadecimal mode, indicating input,output, and console" << endl;;
    cout << "                           should be interpreted in hex rather than binary." << endl;
    cout << endl;
    cout.flush();
}

bool OptParse::parse(int argc, char *argv[]) {
    std::string *args = new std::string[argc];

    for (int i = 0; i < argc; i++) {
        args[i] = std::string(argv[i]);
    }

    return parse(argc, args);
}

bool OptParse::parse(int argc, std::string argv[]) {
    // set all options to initial defaults
    Parsed = Defaults;

    // if there are not modifications, short circuit
    if (!argv || !argc) {
        return true;
    }

    std::string option;

    char scratch[128];

    // loop through all passed in arguments
    for (int i = 1; i < argc; i++) {

        strcpy(scratch, argv[i].c_str());

        option = &scratch[2];

        // check if argument is invalid
        if (scratch[0] != '-') {
#ifdef DEBUG
      cout << "invalid option on command line:" << argv[i] << endl;
      #endif
            printUsage();
            return false;
        }

        // all flags are 1 character long
        switch (scratch[1]) {
            case 'i':
                // set input file
                Parsed.inFile = option;
                continue;

                break;

            case 'o':
                // set output file
                Parsed.outFile = option;
                continue;

                break;

            case 'e':
                // set mode to 'encrypt'
                Parsed.encrypt = true;
                Parsed.decrypt = false;
                continue;

                break;

            case 'd':
                // set mode to 'decrypt'
                Parsed.decrypt = true;
                Parsed.encrypt = false;

                continue;

                break;

            case 'k':
                // set the key manually on command line
                Parsed.keyLoaded = true;
                Parsed.key = option;

                continue;

                break;

            case 'p':
                // set the parameter file
                Parsed.paramFile = option;

                continue;

                break;

            case 's':
                // set versose mode
                Parsed.verbose = true;

                continue;

                break;

            case 'x':
                // set hexadecimal mode
                Parsed.hexadecimal = true;

                continue;

                break;

            default:
                // invalid flag
#ifdef DEBUG
        cout << "invalid command line flag detected: -" << &argv[i][1] << endl;
        #endif

                printUsage();

                return false;

                break;
        }
    }

    return true;
}
