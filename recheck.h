#ifndef _TEST_H_
#define _TEST_H_

/**
* Unit-Testing Framework for C++
*
*   Chris Pergrossi, Feb 2015
*
* a unit testing framework designed to provide an intuitive and easy to follow
* method of implementing TDD - test driven development.  The idea is to create
* the tests for your code BEFORE the code, forcing the developer to:
*     a) write the unit tests (not always an easy task)
*     b) clearly define what the code should do and expected results
*
* additionally, unit tests serve as clear documentation for methods and example
* usage.
*
*     LICENSE: MIT
*
***/

/* EXAMPLE USAGE

stack("Queue Unit Tests");

it("should create a new queue");

  MyQueue Q;

expect(Q.internalPtr).toNotBeNull();

// each it - expect unit is its own scope, so define your tests as independent
// pieces to a larger puzzle rather than make them interdependent.  going for
// clarity and throroughness not speed and optimization.

it("should push an object to the front of the queue");

  MyQueue Q;

  int* base = Q.internalPtr;

  Q.push(9);

expect(*base).toBeEqual(9);

*****************************************************************************/

#include <vector>
#include <string>
#include <stdio.h>

#define ANSI_ERROR      "\x1b[31m"
#define ANSI_WARNING    "\x1b[33m"
#define ANSI_SUCCESS    "\x1b[32m"
#define ANSI_INFO       "\x1b[36m"
#define ANSI_ALL_RESET  "\x1b[0m"

#define ANSI_RED "\x1b[31m"
#define ANSI_GREEN "\x1b[32m"
#define ANSI_YELLOW "\x1b[33m"
#define ANSI_BLUE "\x1b[34m"
#define ANSI_TEAL "\x1b[36m"
#define ANSI_WHITE "\x1b[37m"
#define ANSI_RESET "\x1b[0m"

#define CHECK "\u2713"
#define CROSS "\u2718"
#define DASH  "\u2014"
#define TITLE "\n\x1b[36m  \u2608\x1b[0mecheck Testing Framework\n  -------------------------\n"

#define assert(x)
#define it(x) { _test _Test(std::string(x), &Stack);
#ifdef DEBUG
#define nullptr 0L
#define expect(x) _testresult<typeof x> _Result(x, &_Test); _Result
#else
#define expect(x) _testresult<decltype(x)> _Result(x, &_Test); _Result
#endif
#define stack(x) _teststack Stack(std::string(x));
#define toBeEqual(y) beEqual(y); _Test.resolve(); }
#define toNotBeEqual(y) notBeEqual(y); _Test.resolve(); }
#define toBeNull() beNull(); _Test.resolve(); }
#define toNotBeNull() notBeNull(); _Test.resolve(); }
#define toBeGreaterThan(y) beGreaterThan(y); _Test.resolve(); }
#define toBeLessThan(y) beLessThan(y); _Test.resolve(); }
#define toBeGTE(y) beGTE(y); _Test.resolve(); }
#define toBeLTE(y) beLTE(y); _Test.resolve(); }
#define toBeTrue() beTrue(); _Test.resolve(); }
#define toBeFalse() beFalse(); _Test.resolve(); }

class _test;

class _teststack;

class _test {
protected:
    bool result;

    std::string description;
    _teststack *parent;

public:
    _test(std::string desc, _teststack *p) : description(desc), parent(p) {
    }

    bool resolve();

    bool getResult() {
        return result;
    }

    bool setResult(bool r) {
        result = r;
        return r;
    }

    std::string &getDesc() {
        return description;
    }

    void setDesc(std::string newDesc) {
        description = newDesc;
    }
};

template<class T>
class _testresult {
protected:

    T operand;
    _test *target;

public:

    _testresult(T o, _test *t) : operand(o), target(t) {
    }

    ~_testresult() {
    }

    bool beEqual(T obj) {
        try {return target->setResult(operand == obj);} catch (...) {}
        return false;
    }

    bool notBeEqual(T obj) {
        try {return target->setResult(operand != obj);} catch (...) {}
        return false;
    }

    bool beNull() {
        try {return target->setResult(operand == NULL);} catch (...) {}
        return false;
    }

    bool notBeNull() {
        try {return target->setResult(operand != NULL);} catch (...) {}
        return false;
    }

    bool beGreaterThan(T obj) {
        try {return target->setResult(operand > obj);} catch (...) {}
        return false;
    }

    bool beLessThan(T obj) {
        try {return target->setResult(operand < obj);} catch (...) {}
        return false;
    }

    bool beGTE(T obj) {
        try {return target->setResult(operand >= obj);} catch (...) {}
        return false;
    }

    bool beLTE(T obj) {
        try {return target->setResult(operand <= obj);} catch (...) {}
        return false;
    }

    bool beTrue() {
        try {return target->setResult(operand != nullptr);} catch (...) {}
        return false;
    }

    bool beFalse() {
        try {return target->setResult(operand == nullptr);} catch (...) {}
        return false;
    }
};

class _teststack {
protected:

    std::vector<_test> testGroup;

    bool overall;
    std::string groupName;

public:

    _teststack(std::string name) : overall(true) {
        groupName = name;
    }

    ~_teststack() {
        conclude();
    }

    bool resolve(_test *test) {
        if (!test->getResult())
            overall = false;

        testGroup.push_back(*test);

        return overall;
    }

    bool conclude() {
        if (overall)
            printf("  " ANSI_SUCCESS CHECK ANSI_RESET "  " DASH " " ANSI_INFO "%s\n" ANSI_RESET, groupName.data());
        else
            printf("  " ANSI_ERROR CROSS ANSI_RESET "  " DASH " " ANSI_INFO "%s\n" ANSI_RESET, groupName.data());

        for (std::vector<_test>::iterator it = testGroup.begin(); it != testGroup.end(); ++it) {
            if ((*it).getResult())
                printf("     " ANSI_SUCCESS CHECK ANSI_RESET "  %s\n", (*it).getDesc().data());
            else
                printf("     " ANSI_ERROR CROSS ANSI_RESET "  %s\n", (*it).getDesc().data());
        }

        printf("\n");

        return overall;
    }
};

#endif
