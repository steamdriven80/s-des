#include <stdlib.h>
#include <math.h>

#include "hexbin.h"

#include "streamio.h"
#include "config.h"

#include "logger.h"

#include "keygen.h"

// a catch-all key loading method.  Tests parameter for hexadecimal
// only digits to treat as a key value, otherwise treats a a filename.
// deciphers hexadecimal digits into a u32 value
u32 KeyGen::loadMaster(std::string pathOrValue) {
    u32 value = 0;

    if (pathOrValue.find_first_not_of("AaBbCcDdEeFf0123456789") != std::string::npos) {
        // its a path
        StreamIO file(pathOrValue, false);

        value = strtoul(file.getData(), NULL, 16);
    } else {
        // take it as the key value itself
        value = strtoul(pathOrValue.c_str(), NULL, 16);
    }

    return value;
}

int hex2bin(char hex) {
    int ret = hex - '0';

    if (ret > 0 && ret < 10) return ret;

    ret = hex - 'A';

    if (ret > 0 && ret < 10) return ret + 10;

    ret = hex - 'a';

    if (ret > 0 && ret < 10) return ret + 10;

    // invalid character!
    return 0;
}

u32 rotateLeft(u32 value, u32 count, u32 bitCount) {

    u32 mask = ((int) pow(2, bitCount) - 1);

    std::string A = Bin2sBin(value, bitCount),
            B = Bin2sBin(mask, bitCount),
            C = Bin2sBin((value << count | (value >> (bitCount - count))) & mask, bitCount);

    Logger::log(LOGLEVEL_VERBOSE, "Rotating Left: '" + A + "' and mask '" + B + "' " + itoa(count) + " places ->'" + C + "'\n");

    return ((value << count) | (value >> (bitCount - count))) & mask;
}

// generates all the round keys from the passed in master key with provided functions
void KeyGen::generate(u32 master, u32 numberRounds, std::string initialPermute, std::string rotSchedule, std::string roundPermute) {

    release();

    m_rounds = new u32[numberRounds];
    m_master = master;

    LookupTable PC1, PC2;

    PC1.permutation = initialPermute;
    PC2.permutation = roundPermute;

    int bitCount = ceil(initialPermute.length() / 2.0);

    u32 roundMaster = PC1.permute(master);

    int left, right, lmask, rmask, roundKey = roundMaster;

    Logger::log(LOGLEVEL_VERBOSE, "Generating Round Keys with Master Key '" + Bin2sHex(roundMaster) + "'\n");

    rmask = pow(2, bitCount) - 1;
    lmask = rmask << (bitCount);

    for (u32 round = 0; round < numberRounds; round++) {

        left = roundKey & lmask;
        right = roundKey & rmask;

        left = rotateLeft(left >> (bitCount), rotSchedule[round] - '0', bitCount);
        right = rotateLeft(right, rotSchedule[round] - '0', bitCount);

        roundKey = ((left << (bitCount)) & lmask) | (right & rmask);

        Logger::log(LOGLEVEL_VERBOSE, "   Round " + itoa(round + 1) + " Key is '" + Bin2sBin(roundKey, bitCount * 2) + "', After PC: '" + Bin2sBin(PC2.permute(roundKey), bitCount * 2) + "'\n");

        m_rounds[round] = PC2.permute(roundKey);
    }

}

// releases allocated memory
void KeyGen::release() {
    if (m_rounds) {
        delete[] m_rounds;
        m_rounds = NULL;
        m_master = 0;
        m_numRounds = 0;
    }
}

std::string KeyGen::getMastersHex() {
    return Bin2sHex(m_master);
}

std::string KeyGen::getMastersBin() {
    return Bin2sBin(m_master);
}

// retrieves individual round keys in various forms
u32         KeyGen::getRoundKeyBin(u32 index) {
    return m_rounds[index];
}

std::string KeyGen::getRoundKeysHex(u32 index) {
    return Bin2sHex(m_rounds[index]);
}

std::string KeyGen::getRoundKeysBin(u32 index) {
    return Bin2sBin(m_rounds[index]);
}
