#include <math.h>

#include "mangler.h"

Mangler::Mangler() {
    m_latestBlock = NULL;
    m_blockSize = 0;
}

Mangler::~Mangler() {
    release();
}

void Mangler::init(int argc, char *argv[]) {

    m_options.loadDefaults();

    m_options.parse(argc, argv);

    m_config.load("test-params.txt");

    setMasterKey(m_keys.loadMaster(m_options.options().key));
}

void Mangler::release() {
    if (m_latestBlock) {
        delete[] m_latestBlock;

        m_latestBlock = NULL;
        m_blockSize = 0;
    }
}

void Mangler::setMasterKey(unsigned int key) {
    Logger::log(LOGLEVEL_VERBOSE, "Generating Round Keys...\n");
    m_keys.generate(key, (u32) m_config.roundCount, m_config.pcRoundInitial.permutation, m_config.leftShiftSchedule.permutation, m_config.pcRoundKey.permutation);;
}

u32 feistel(u32 key, u32 block, Config &config) {
    u32 bitcount = config.expansionPermute.permutation.length();
    Logger::log(LOGLEVEL_VERBOSE, "Feistel Mangling: Parameter: " + Bin2sBin(block, bitcount) + "\n");
    block = config.expansionPermute.permute(block) ^ key;
    Logger::log(LOGLEVEL_VERBOSE, "      After Expanding and XOR w/ Key: " + Bin2sBin(block, bitcount) + "\n");

    u32 newBlock = 0;
    u32 pieceSize = (u32) config.expansionPermute.permutation.length() / config.sboxCount;

    Logger::log(LOGLEVEL_VERBOSE, "      Then broken into " + itoa(pieceSize) + "bit pieces.\n");

    u32 flag = (u32) pow(2, pieceSize) - 1;

    for (int i = 0; i < config.sboxCount; i++) {
        Logger::log(LOGLEVEL_DEBUG, "  Argument to CherryPick Round " + itoa(i) + ": " + Bin2sBin((block & flag) >> (i * pieceSize), bitcount) + "\n");
        newBlock |= config.sBoxes[i].cherryPick((block & flag) >> (i * pieceSize)) << (i * 2);
        flag = flag << (pieceSize);
    }

    Logger::log(LOGLEVEL_VERBOSE, "  Final Result Pre-Exit Permutation: " + Bin2sBin(newBlock, bitcount) + "\n");

    u32 pBox = config.pBox.permute(newBlock);

    Logger::log(LOGLEVEL_VERBOSE, "  After PBox Transposition: " + Bin2sBin(pBox, bitcount / 2) + "\n");

    return pBox;
}

u32 Mangler::encryptBlock(u32 Block) {
    u32 left = 0, right = 0;
    u32 nleft = 0, nright = 0;
    u32 lmask = 0, rmask = 0;

    u32 BitCount = (u32) m_config.blockSize;
    int round = 0;

    Block = Block & ((u32) pow(2, BitCount) - 1);

    rmask = ((u32) pow(2, BitCount / 2)) - 1;
    lmask = rmask << (BitCount / 2);

    Logger::log(LOGLEVEL_VERBOSE, "Block to be encrypted: " + Bin2sBin(Block, BitCount) + "\n");

    Block = m_config.initialPermute.permute(Block);

    Logger::log(LOGLEVEL_VERBOSE, "   Post-Initial Permutation: " + Bin2sBin(Block, BitCount) + "\n");

    left = Block & lmask;
    right = Block & rmask;

    for (round = 0; round < m_config.roundCount; round++) {
        nleft = right;
        nright = left ^ feistel(m_keys.getRoundKeyBin(round), right, m_config);

        Logger::log(LOGLEVEL_VERBOSE, " Round #" + itoa(round) + " Left: " + Bin2sBin(nleft, BitCount / 2) + " Right: " + Bin2sBin(nright, BitCount / 2) + "\n");

        left = nleft;
        right = nright;
    }

    left = nright;
    right = nleft;

    Block = left << (BitCount / 2) | right;

    Logger::log(LOGLEVEL_VERBOSE, " After all Rounds, Block is " + Bin2sBin(Block) + "\n");

    Block = m_config.inverseIP.permute(Block);

    Logger::log(LOGLEVEL_VERBOSE, "  Post-Inverse Permutation: " + Bin2sBin(Block) + "\n");

    return Block;
}

u32 Mangler::decryptBlock(u32 Block) {
    u32 left = 0, right = 0;
    u32 nleft = 0, nright = 0;
    u32 lmask = 0, rmask = 0;

    u32 BitCount = (u32) m_config.blockSize;
    int round = 0;

    Block = Block & ((u32) pow(2, BitCount) - 1);

    rmask = ((u32) pow(2, BitCount / 2)) - 1;
    lmask = rmask << (BitCount / 2);

    Logger::log(LOGLEVEL_VERBOSE, "Block to be encrypted: " + Bin2sBin(Block, BitCount) + "\n");

    Block = m_config.initialPermute.permute(Block);

    Logger::log(LOGLEVEL_VERBOSE, "   Post-Initial Permutation: " + Bin2sBin(Block, BitCount) + "\n");

    left = Block & lmask;
    right = Block & rmask;

    for (round = 0; round < m_config.roundCount; round++) {
        nleft = right;
        nright = left ^ feistel(m_keys.getRoundKeyBin(m_config.roundCount - 1 - round), right, m_config);

        Logger::log(LOGLEVEL_VERBOSE, " Round #" + itoa(round) + " Left: " + Bin2sBin(nleft, BitCount / 2) + " Right: " + Bin2sBin(nright, BitCount / 2) + "\n");

        left = nleft;
        right = nright;
    }

    left = nright;
    right = nleft;

    Block = left << (BitCount / 2) | right;

    Logger::log(LOGLEVEL_VERBOSE, " After all Rounds, Block is " + Bin2sBin(Block) + "\n");

    Block = m_config.inverseIP.permute(Block);

    Logger::log(LOGLEVEL_VERBOSE, "  Post-Inverse Permutation: " + Bin2sBin(Block) + "\n");

    return Block;
}
