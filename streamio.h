#ifndef _STREAM_IO_H_
#define _STREAM_IO_H_

/**
*   streamio.h
*
*   Chris Pergrossi - CIS4362
*
* this object manages input and output to/from file streams both in binary
* and ASCII Hexadecimal.
*
***/

#include <string>

class StreamIO {
protected:

    friend void StreamIO_Tests();

    bool m_binary;

    std::string m_fileName;

    char *m_data;
    int m_size;

public:

    StreamIO() : m_binary(true), m_fileName(""), m_data(NULL), m_size(0) {
    }

    StreamIO(std::string file, bool isBinary = true) : m_binary(true), m_fileName(""), m_data(NULL), m_size(0) {
        load(file, isBinary);
    }

    ~StreamIO() {
        close();
    }

    // loads a file into memory
    bool load(std::string file, bool isBinary = true);

    // frees memory allocated for a file
    bool close();

    // sets the pointer to point to the internal map of the file
    // WARNING: there is only 1 copy of the file in memory, modifying it will
    // affect other code that references the same file!
    //
    //  @retval : the size of the file in bytes
    int getData(char **buffer) {
        *buffer = m_data;
        return m_size;
    }

    char *getData() {
        return m_data;
    }

    std::string getDataStr() {
        return std::string(m_data);
    }

    // returns true if a file is loaded into memory
    bool isLoaded() {
        return m_data != NULL;
    }

    std::string getFileName() {
        return m_fileName;
    }
};

#endif
